<?php

    require_once "Cliente.php";

    class Conta extends Cliente{
        
        public $numeroConta;

        public function ImprimirConta(){
            echo "NumeroConta: " . $this->numeroConta . "<br>";
            echo "Cliente: " . $this->nome . "<br>";
        }

    }

    $cliente1 = new Conta();
    $cliente1->nome = "Matheus Francisco";
    $cliente1->numeroConta = "1235451";
    $cliente1->ImprimirConta();
?>