<?php
    class Cliente{

        private $codigo;
        public $nome;
        public $idade;
        protected $cpf;

        public function setCpf($cpf){
            //validacao
            $this->cpf = $cpf;
        }

        public function ImprimirDados(){
            echo "Nome: " . $this->nome . "<br>";
            echo "Idade: " . $this->idade . "<br>";
            echo "CPF: " . $this->cpf . "<br>";
        }

    }

    // $cliente1 = new Cliente();
    // $cliente1->nome = "Matheus Francisco";
    // $cliente1->idade = 28;
    // $cliente1->setCpf(("12345678"));

    // $cliente2 = new Cliente();
    // $cliente2->nome = "João Paulo";
    // $cliente2->idade = "39";
    // $cliente2->setCpf("87654321");

    // $cliente1->ImprimirDados();
    // echo "<hr>";
    // $cliente2->ImprimirDados();
?>