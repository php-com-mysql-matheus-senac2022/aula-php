<?php
    echo "<h1>Estrutura Condicional</h1>";

    $nome = "Matheus Francisco";
    $idade = 20;
    $email = "edson@teste.com";
    $senha = "12345678";

    echo "<h4>... if()...</h4>";

    if ($idade >= 18){
        echo "O usuário $nome é maior de idade";
    }

    echo "<h4>... if() e else ...</h4>";

    $idade = 17;


    if ($idade >= 18){
        echo "O usuário $nome é maior de idade";
    }else{
        echo "O usuário $nome é menor de idade";
    }



echo "<h4>Exemplo de condição para Login</h4>";

if($email == "matheus@teste.com" && $senha == "12345678"){
    echo "Usuário Logado";

}else{
    echo "Usuário ou senha inválido";
}

echo "<hr>";
////////////////////////////////////////////
echo "<h4>...if() else elseif() else </h4>";

$idade = 11;

if($idade >= 18){
    echo "O usuário $nome é Adulto";
}else if($idade >= 12){
    echo "O usuário $nome é Adolescente";
}else{
    echo "O usuário $nome é Criança";
}

echo "<hr>";
////////////////////////////////////////////
echo "<h4>If ternário</h4>";

echo ($idade >= 18) ? "O usuário $nome é maior de idade" : "O usuário $nome é menor de idade";
//$maiorDeIdade = $idade >= 18; retorna true ou false

echo "<hr>";
////////////////////////////////////////////
echo "<h4>Validação Variável</h4>";

$txtNome = "Matheus";
echo $txtNome ?? 'Não tem nome';

//$nome = $txtNome ?? 'default'; atribui um valor default se a variavel for vazia.

echo "<hr>";
////////////////////////////////////////////
echo "<h4>Condição com switch case</h4>";
$menu = 'Home';    
switch($menu){

        case 'Home':
            echo 'Clicou no menu Home';
            break;
        case 'Empresa':
            echo 'Clicou no menu Empresa';
            break;
        case 'Contato':
            echo 'Clicou no menu Contato';
            break;
        default:
            echo 'Nenhuma das opções do menu';
    }
?>