<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Formulário</title>
</head>
<body>
    <h1>Envio por GET</h1>
    <form action="aula6.php" method="get">
        <p>
            <label>Nome: </label>
            <input type="text" name="nome">
        </p>

        <p>
            <label>Idade: </label>
            <input type="text" name="idade">
        </p>

        <p>
            <button type="submit">Cadastrar</button>
        </p>
    </form>
    <?php
        if(isset($_GET['nome'])&& isset($_GET['idade'])){
        echo $_GET['nome'];
        echo "<br>";
        echo $_GET['idade'];
    }
    ?>

<h2>Envio por POST</h2>
    <form action="aula6.php" method="post">
        <p>
            <label>Nome: </label>
            <input type="text" name="nome">
        </p>

        <p>
            <label>Idade: </label>
            <input type="text" name="idade">
        </p>

        <p>
            <button type="submit">Cadastrar</button>
        </p>
    </form>

    <?php
        if(isset($_POST['nome'])&& isset($_POST['idade'])){
            echo $_POST['nome'];
            echo "<br>";
            echo $_POST['idade'];
        }
    ?>

</body>
</html>