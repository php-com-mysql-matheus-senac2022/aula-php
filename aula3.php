<!DOCTYPE html>
<hmtl>  
    <head> 
        <meta charset="utf-8">
        <title>Exemplo de Java Script</title>
    </head>
    <body>
        
        <a href="javascript:;" id="btnAlerta">Mostrar Alerta em JavaScript</a> 

        <br>

        <a href="https://google.com.br" target="_blank">Google</a>

        <script>
            var alerta = document.getElementById("btnAlerta");
                alerta.onclick = function(){
                    alert('Este é um exemplo de JavaScript');
                }
        </script> 
    </body>
</html>