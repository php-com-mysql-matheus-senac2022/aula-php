<?php

$listaCompra = ["banana", "arroz", "feijão"];

echo $listaCompra[1];

echo "<hr>";

/*echo "<pre>";
var_dump($listaCompra);
echo "</pre>";*/

echo "<pre>";
print_r($listaCompra);
echo "</pre>";

echo "<hr>";
$usuario = [
    "nome" => "Matheus Francisco",
    "email" => "matheus@teste.com",
];
echo $usuario['nome'];

echo "<hr>";
$usuarios = [
    [
        "nome" => "Matheus Francisco",
        "email" => "matheus@teste.com",
    ],
    [
        "nome" => "João",
        "email" => "joao@teste.com",
    ]  
    
    ];

echo $usuarios[1]['nome'];

echo "<hr>";

foreach($listaCompra as $item){
    echo $item . ", ";
}

echo "<hr>";

// [0] => ['nome' => '...']
foreach($usuarios as $key => $usuario){
    echo $key . " - ";
    echo $usuario['nome']."<br>";
    echo $usuario['email']."<br>";
}

