<?php
    function buscarEndereco($cep){
        $endereco = file_get_contents("http://viacep.com.br/ws/$cep/json/");
        return json_decode($endereco, true);
    }

    $endereco = buscarEndereco("14401373");

    /*echo "<pre>";
    print_r($endereco);
    echo "</pre>";*/

    echo "Endereço: " . $endereco['logradouro'] . "<br>";
    echo "Bairro: " . $endereco['bairro'] . "<br>";
    echo "Cidade: " . $endereco['localidade'] . "<br>";
    echo "Estado: " . $endereco['uf'] . "<br>";
    echo "CEP: " . $endereco['cep'] . "<br>";
 ?>
